package com.sreesuhanelectricals.sseautomation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SseAutomationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SseAutomationApplication.class, args);
	}

}
